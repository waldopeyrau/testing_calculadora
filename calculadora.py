# -*- coding: cp1252 -*-
# v3.7


class Calculadora:
    def __init__(self):
        self.x = 0
        self.y = 0

    def set_x(self,x):
        self.x = x

    def get_x(self):
        return self.x

    def set_y(self,y):
        self.y = y

    def get_y(self):
        return self.y
    
    def sumar(self,x,y):
        print  ("\nLa suma de "+ str(x) +" y "+ str(y) +" es: "+ str(float(x+y)))

    def restar(self,x,y):
        print  ("\nLa resta de "+ str(x) +" y "+ str(y) +" es: "+ str(float(x-y)))

    def multiplicar(self,x,y):
        print  ("\nLa multiplicación de "+ str(x) +" y "+ str(y) +" es: "+ str(float(x*y)))

    def dividir(self,x,y):
        if (y == 0):
            print ("\nError matematico, no se puede dividir por cero\n")
        else:
            print  ("\nLa división de "+ str(x) +" y "+ str(y) +" es: "+ str(float(x/y)))

    def raiz(self,x):
        if (x == 0):
            print ("\nLa raiz de cero es",0,"\n")
        else:
            print  ("\nLa raiz cuadrada de "+ str(x) + " es: "+ str(float(x**(1/2))))

    def raizn(self,x,y):
        if (x == 0):
            print ("\nLa raiz de cero es",0,"\n")
        else:
            print  ("\nLa raiz cuadrada de "+ str(x) + " es: "+ str(float(x**(1/y))))

    def potencia(self,x,y):
        if (y == 0):
            print ("El resultado de su potencia es: ",1)
        else:
            print  ("\nEl resulta de "+ str(x) +" elevado a "+ str(y) +" es: "+ str(float(x**(y))))

    def lee_numero(self):
        while True:
            numero = input("\nIngrese un numero : ")
            try:
                numero = int(numero)
                return numero
            except ValueError:
                print ("**********************************************")
                print ("**            INGRESE UN NUMERO              **")
                print ("**********************************************")

                

                
        
    
class Menu():
    def Opciones(self):

        print ("\n#######################################")
        print ("\n######## CALCULADORA TESTING  #########\n")
        print ("#######################################")
        print ("\nSELECCIONE UNA OPCION: \n")
        print ("1 - SUMAR")
        print ("2 - RESTAR")
        print ("3 - MULTIPLICAR")
        print ("4 - DIVIDIR")
        print ("5 - RAIZ")
        print ("6 - POTENCIA")
        print ("7 - RAIZ N")
        print ("0 - SALIR")


        while True:
            entrada = input("\nIntroduce la opción: ")
            try:
                entrada = int(entrada)
                return entrada
            except ValueError:
                print ("**********************************************")
                print ("** ERROR, LA OPCION INGRESADA NO ES VALIDA  **")
                print ("**********************************************")
                print ("SELECCIONE UNA OPCION: \n")
                print ("1 - SUMAR")
                print ("2 - RESTAR")
                print ("3 - MULTIPLICAR")
                print ("4 - DIVIDIR")
                print ("5 - RAIZ")
                print ("6 - POTENCIA")
                print ("7 - RAIZ N")
                print ("0 - SALIR")


C = Calculadora()

while (True):
    
    M = Menu()

    opt = M.Opciones()

    
    if (opt == 1 or opt == 2 or opt == 3 or opt == 4 or opt == 5 or opt == 6 or opt == 7):

        #NUMX = C.lee_numero()
        #C.set_x(NUMX)
        #NUMY = C.lee_numero()
        #C.set_y(NUMY)
        
        
        if (opt == 1):
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.sumar(C.get_x(),C.get_y())

        elif (opt == 2):
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.restar(C.get_x(),C.get_y())

        elif (opt == 3):
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.multiplicar(C.get_x(),C.get_y())

        elif (opt == 4):
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.dividir(C.get_x(),C.get_y())
        

        elif (opt == 5):

            NUMX = C.lee_numero()
            C.set_x(NUMX)
            C.raiz(C.get_x())

        elif (opt == 6):
            print("\nIngrese la Base")
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            print("\nIngrese el Exponente")
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.potencia(C.get_x(),C.get_y())

        elif (opt == 7):
            NUMX = C.lee_numero()
            C.set_x(NUMX)
            print("\nIngrese nraiz")
            NUMY = C.lee_numero()
            C.set_y(NUMY)
            C.raizn(C.get_x(),C.get_y())


    elif (opt == 0):
        break
            
    else:
        print ("**********************************************")
        print ("** ERROR, LA OPCION INGRESADA NO ES VALIDA  **")
        print ("**********************************************")

