from tkinter import*

def botClick(numeros):
	global operator
	operator=operator + str(numeros)
	text_I.set(operator)

def botLimpiar():
	global operator
	operator=""
	text_I.set("")

def botIgual():
	global operator
	result=str(eval(operator))
	text_I.set(result)
	operator=""



cal = Tk()
cal.title("Calculadora")
operator =""
text_I =StringVar()

txtDisplay = Entry (cal, font=('arial', 20, 'bold'), textvariable = text_I, bd=30, insertwidth = 4, bg = "powder blue", justify = 'right').grid(columnspan=4)

botRaiz2 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 12, 'bold'), text ="rz2", bg = "powder blue", command=lambda:botClick("rz2")).grid(row=1,column=0)  

botPot = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 12, 'bold'), text ="pot", bg = "powder blue", command=lambda:botClick("**")).grid(row=1,column=1)  

botRaizN = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 12, 'bold'), text ="rzN", bg = "powder blue", command=lambda:botClick("rzN")).grid(row=1,column=2)  

botDiv = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="/", bg = "powder blue", command=lambda:botClick("/")).grid(row=1,column=3)  
#======================================================================================================================================================
bot7 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="7", bg = "powder blue", command=lambda:botClick(7)).grid(row=2,column=0)  

bot8 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="8", bg = "powder blue", command=lambda:botClick(8)).grid(row=2,column=1)  

bot9 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="9", bg = "powder blue", command=lambda:botClick(9)).grid(row=2,column=2)  

botMult = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="*", bg = "powder blue", command=lambda:botClick("*")).grid(row=2,column=3)  
#======================================================================================================================================================
bot4 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="4", bg = "powder blue", command=lambda:botClick(4)).grid(row=3,column=0)  

bot5 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="5", bg = "powder blue", command=lambda:botClick(5)).grid(row=3,column=1)  

bot6 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="6", bg = "powder blue", command=lambda:botClick(6)).grid(row=3,column=2)  

botResta = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 22, 'bold'), text ="-", bg = "powder blue", command=lambda:botClick("-")).grid(row=3,column=3)  
#======================================================================================================================================================
bot1 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="1", bg = "powder blue", command=lambda:botClick(1)).grid(row=4,column=0)  

bot2 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="2", bg = "powder blue", command=lambda:botClick(2)).grid(row=4,column=1)  

bot3 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="3", bg = "powder blue", command=lambda:botClick(3)).grid(row=4,column=2)  

botSuma = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 19, 'bold'), text ="+", bg = "powder blue", command=lambda:botClick("+")).grid(row=4,column=3)  
#======================================================================================================================================================
bot0 = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="0", bg = "powder blue", command=lambda:botClick(0)).grid(row=5,column=0)  

botPto = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text =" .", bg = "powder blue").grid(row=5,column=1)  

botC = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="C", bg = "powder blue", command=botLimpiar).grid(row=5,column=2)  

botIgual = Button(cal, padx=16, bd= 8, fg="black", font=('arial', 20, 'bold'), text ="=", bg = "powder blue", command=botIgual).grid(row=5,column=3)  

cal.mainloop()
